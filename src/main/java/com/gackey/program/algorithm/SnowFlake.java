package com.gackey.program.algorithm;

/**
 * @author gackey
 * @desc 雪花算法
 * @className SnowFlake
 * @date 2022/6/10 7:40
 */
public class SnowFlake {

    //机器码（5位）
    private static long workerId;
    //数据中心序号（5位）
    private static long datacenterId;
    //每毫秒生产的序列号从0开始递增；
    private static long sequence = 0L;
    //起始时间
    private static long twepoch = 1288834974657L;

    private static long workerIdBits = 5L;

    private static long datacenterIdBits = 5L;

    private long maxWorkerId = -1L ^ (-1L << workerIdBits);

    private long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);

    private static long sequenceBits = 12L;

    private static long workerIdShift = sequenceBits;

    private static long datacenterIdShift = workerIdShift + workerIdBits;

    private static long timestampLeftShift = datacenterIdShift + datacenterIdBits;

    private static long sequenceMask = -1L ^ (-1L << sequenceBits);

    private static long lastTimestamp = -1L;

    //无参构造
    public SnowFlake() {
        this(1, 1);
    }

    public SnowFlake(long datacenterId, long workerId) {
        if ((datacenterId > maxDatacenterId || datacenterId < 0) || (workerId > maxWorkerId || workerId < 0)) {
            throw new IllegalArgumentException("datacenterId or workerId is illegal");
        }
        SnowFlake.datacenterId = datacenterId;
        SnowFlake.workerId = workerId;

    }

    //通过SnowFlake生成id的核心算法
    public static synchronized long nextId() {
        long timestamp = System.currentTimeMillis();
        if (timestamp < lastTimestamp) {
            // 时间戳值非法
            throw new IllegalStateException("The timestamp is invalid.");
        }
        //如果此次生成id的时间戳，与上次的时间戳相同，就通过机器码和序列号区分id值（机器码已通过构造方法传入）
        if (lastTimestamp == timestamp) {
        /*
        通过位运算保证sequence不会超出序列号所能容纳的最大值。
        例如，本程序产生的12位sequence值依次是：1、2、3、4、...、4094、4095（4095是2的12次方的最大值，也是本sequence的最大值）
        那么此时如果再增加一个sequence值（即sequence + 1），下条语句就会使sequence恢复到0。即如果sequence==0，就表示sequence已满。
        */
            sequence = (sequence + 1) & sequenceMask;
            //如果sequence已满，就无法再通过sequence区分id值；因此需要切换到下一个时间戳重新计算。
            if (sequence == 0) {
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            //如果此次生成id的时间戳，与上次的时间戳不同，就已经可以根据时间戳区分id值
            sequence = 0L;
        }
        //更新最近一次生成id的时间戳
        lastTimestamp = timestamp;
        return ((timestamp - twepoch) << timestampLeftShift) | (datacenterId << datacenterIdShift) | (workerId << workerIdShift) | sequence;
    }

    protected static long tilNextMillis(long lastTimestamp) {
        long timestamp = System.currentTimeMillis();
        /*
        如果当前时刻的时间戳<=上一次生成id的时间戳，就重新生成当前时间。
        即确保当前时刻的时间戳，与上一次的时间戳不会重复。
        */
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }
}
