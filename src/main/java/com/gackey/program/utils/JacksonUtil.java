/**
 * title: JacksonUtil.java
 * copyleft:©2020-2021 gackey. All Rights Reserved.
 */
package com.gackey.program.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.json.JsonParseException;

import java.util.concurrent.Callable;

/**
 * @desc jackson工具
 * @className JacksonUtil
 * @author gackey
 * @date 2021-09-05 18:07
 */
public class JacksonUtil {

    private static final ThreadLocal<ObjectMapper> LOCAL_JSON = new ThreadLocal<ObjectMapper>() {

        @Override
        protected ObjectMapper initialValue() {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
            objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
            return objectMapper;
        }
    };

    public static ObjectMapper getObjMapper() {
        return LOCAL_JSON.get();
    }

    /**
     * 捕捉异常
     * @param parser
     * @return
     * @param <T>
     * @usage String jsonStr = JacksonUtil.tryParse(() -> JacksonUtil.getObjMapper().writeValueAsString("{Json String}"));
     */
    public static <T> T tryParse(Callable<T> parser){
        return tryParse(parser, JacksonException.class);
    }

    public static <T> T tryParse(Callable<T> parser, Class<? extends Exception> check){
        try {
            return parser.call();
        } catch (Exception ex) {
            if (check.isAssignableFrom(ex.getClass())){
                throw new JsonParseException(ex);
            }
            throw new IllegalStateException(ex);
        }
    }
}
