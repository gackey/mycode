/*
 * title: TemplateMethodTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.templatemethodpattern;

/**
 * desc: 模板方法  定义一个操作中的算法的骨架，而将一些步骤延迟到子类中，模板方法使得子类可以不改变一个算法的结构即可以重定义该算法的某些特定步骤。
 * className: TemplateMethodTest
 * author: gackey
 * date: 2018-11-29 21:19
 */
public class TemplateMethodTest {

    public static void main(String[] args) {
        System.out.println("***学生甲的试卷：");
        TestPaperA studentA = new TestPaperA();
        studentA.testQuestion1();
        studentA.testQuestion2();
        studentA.testQuestion3();

        System.out.println("***学生乙的试卷：");
        TestPaperB studentB = new TestPaperB();
        studentB.testQuestion1();
        studentB.testQuestion2();
        studentB.testQuestion3();
    }

}
