/*
 * title: StrategyPatternTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.strategypattern;

/**
 * Desc: 策略模式  它定义了算法家族，分别封装起来，让他们之间可以互相替换，此模式让算法的变化，不会影响到使用算法的客户。
 * ClassName: StrategyPatternTest
 * author: gackey
 * date: 2018-11-24 13:58
 */
public class StrategyPatternTest {

    public static void main(String[] args) {
        double money = 700.50;
        String type = "打8折";

        CashContext cc = new CashContext(type);
        double result = cc.getResult(money);

        System.out.println("参与优惠后合计" + result + "元");

    }

}
