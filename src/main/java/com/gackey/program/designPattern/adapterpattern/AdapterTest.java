/*
 * title: AdapterTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.adapterpattern;

/**
 * @desc: 适配器模式  将一个类的接口转换成客户希望的另外一个接口，Adapter模式使得原本由于接口不兼容而不能一起工作的那些类可以一起工作。
 * @className: AdapterTest
 * @author: gackey
 * @date: 2018-12-08 19:43
 */
public class AdapterTest {

    public static void main(String[] args) {
        Player guards = new Guards("麦迪");
        Player forwards = new Forwards("巴蒂尔");
        Player center = new Translator("姚明");

        guards.attack();
        forwards.attack();
        center.attack();
        System.out.println("-----------------------");
        guards.defense();
        forwards.defense();
        center.defense();
    }

}
