/*
 * title: ObserverTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.observerpattern;

/**
 * Desc: 观察者模式  定义了一种一对多的依赖关系，让多个观察者对象同时监听某一个主题对象。这个主题对象在状态发生变化时，会通知所有观察者对象，使他们能够自动更新自己。
 * ClassName: ObserverTest
 * author: gackey
 * date: 2018-11-25 12:56
 */
public class ObserverTest {

    public static void main(String[] args) {
        Secretary sakura = new Secretary();
        Boss kakashi = new Boss();

        StockObserver sasuke = new StockObserver("佐助", sakura);
        NBAObserver naruto = new NBAObserver("鸣人", sakura);
        sakura.subAttach(sasuke);
        sakura.subAttach(naruto);
        sakura.setSubjectStatu("卡卡西老师来了！小樱去通知大家。");
        sakura.subNotify();

        System.out.println("------------------------");

        StockObserver sasuke2 = new StockObserver("佐助", kakashi);
        NBAObserver naruto2 = new NBAObserver("鸣人", kakashi);
        kakashi.subAttach(sasuke2);
        kakashi.subAttach(naruto2);
        // 卡卡西用神威突然出现，鸣人没有察觉到，不被通知
        kakashi.subDetach(naruto2);
        kakashi.setSubjectStatu("卡卡西用神威突然出现，大家还没有察觉。");
        kakashi.subNotify();
    }

}
