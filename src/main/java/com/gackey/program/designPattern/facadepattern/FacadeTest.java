/*
 * title: FacadeTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.facadepattern;

/**
 * @desc: 外观模式  为子系统中的一组接口提供一致的界面，此模式定义了一个高层接口，这个接口使得这一子系统更加容易使用。
 * @className: FacadeTest
 * @author: gackey
 * @date: 2018-12-08 20:17
 */
public class FacadeTest {

    public static void main(String[] args) {
        Fund fund = new Fund();
        fund.buyFund();
        System.out.println("-------------------");
        fund.sellFund();
    }

}
