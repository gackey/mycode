/*
 * title: DecoratorTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.decoratorpattern;

/**
 * @desc: 装饰模式  动态地给一个对象添加一些额外的职责，就增加的功能来说，装饰模式比生成子类更加灵活。
 * @className: DecoratorTest
 * @author: gackey
 * @date: 2018-12-08 08:55
 */
public class DecoratorTest {

    public static void main(String[] args) {
        ConcreteComponent component = new ConcreteComponent();
        ConcreteDecoratorA cda = new ConcreteDecoratorA();
        ConcreteDecoratorB cdb = new ConcreteDecoratorB();

        cda.setComponent(component);
        cdb.setComponent(cda);
        cdb.operation();
    }

}
