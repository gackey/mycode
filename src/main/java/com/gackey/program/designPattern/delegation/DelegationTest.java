/*
 * title: DelegationTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.delegation;

import java.util.Date;

/**
 * Desc: 委派模式 属于行为型模式，不属于GOF 23中设计模式之一。
 *      负责任务的调度和分配任务，跟代理模式很像，可以看作是一种特殊情况下的静态代理的全权代理，但是代理模式注重过程，而委派模式注重结果。
 * ClassName: DelegationTest
 * author: gackey
 * date: 2018-11-28 22:54
 */
public class DelegationTest {

    public static void main(String[] args) {
        GoodNotifier goodNotifier = new GoodNotifier();

        WatchCartoonListener wcListener = new WatchCartoonListener();
        PlayingGameListener pgListener = new PlayingGameListener();
        Date date = new Date();

        goodNotifier.addListener(wcListener, "stopWatchCartoon", date);
        goodNotifier.addListener(pgListener, "stopPlayingGame", date);

        try {
            // 3秒钟后老师来了
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        goodNotifier.notifyX();
    }

}
