/*
 * title: CommandTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.commandpattern;

/**
 * @desc: 命令模式  将一个请求封装为一个对象，从而使你可以用不同的请求对客户进行参数化；对请求排队或记录请求日志，以及支持可撤销的操作。
 * @className: CommandTest
 * @author: gackey
 * @date: 2018-12-01 14:33
 */
public class CommandTest {

    public static void main(String[] args) {
        // 烧烤店营业
        Barbecuer boy = new Barbecuer();
        BakeMuttonCommand bm1 = new BakeMuttonCommand(boy);
        BakeMuttonCommand bm2 = new BakeMuttonCommand(boy);
        BakeChickenWingCommand bcw1 = new BakeChickenWingCommand(boy);
        Waiter waiter = new Waiter();

        // 顾客点餐,两打羊肉串，一打鸡翅
        waiter.addOrder(bm1);
        waiter.addOrder(bm2);
        waiter.addOrder(bcw1);
        // 取消一份羊肉串
        waiter.cancelOrder(bm2);

        // 通知后台做菜
        System.out.println("--------服务员通知厨房-----------");
        waiter.notifyExcute();
    }

}
