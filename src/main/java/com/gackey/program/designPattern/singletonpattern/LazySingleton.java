/*
 * title: LazySingleton.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.singletonpattern;

/**
 * Desc: 单例模式 - 懒汉式
 * ClassName: LazySingleton
 * author: gackey
 * date: 2018-11-04 00:21
 */
public class LazySingleton {

    private static volatile LazySingleton instance = null;

    private LazySingleton() {
    }

    public static LazySingleton getInstance() {
        //双重检查锁定（Double-Checked Locking）保证在多线程环境下，单例模式的唯一性
        if (instance == null) {
            synchronized (LazySingleton.class) {
                if (instance == null) {
                    instance = new LazySingleton();
                }
            }
        }
        return instance;
    }
}
