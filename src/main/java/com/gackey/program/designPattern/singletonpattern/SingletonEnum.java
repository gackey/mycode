/*
 * title: SingletonEnum.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.singletonpattern;

/**
 * Desc: 单例模式
 * ClassName: SingletonEnum
 * author: gackey
 * date: 2018-11-04 15:07
 * 单例模式可以通过反射被攻击，最理想的不会受到攻击的单例模式实现是借助 Java 里枚举类 Enumeration 的特性
 */
public enum SingletonEnum {

    INSTANCE;

    public SingletonEnum getInstance() {
        return SingletonEnum.INSTANCE;
    }
}
