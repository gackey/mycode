/*
 * title: HungrySingleton.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.singletonpattern;

/**
 * Desc: 单例模式 - 饿汉式
 * ClassName: HungrySingleton
 * author: gackey
 * date: 2018-11-04 15:05
 */
public class HungrySingleton {

    private static HungrySingleton instance = new HungrySingleton();

    private HungrySingleton() {
    }

    public static HungrySingleton getInstance() {
        return instance;
    }
}
