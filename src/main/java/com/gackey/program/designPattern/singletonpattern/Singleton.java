/*
 * title: Singleton.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.singletonpattern;

/**
 * Desc: 单例模式  保证一个类只有一个实例，并提供一个访问它的全局访问点 。
 * ClassName: Singleton
 * author: gackey
 * date: 2018-11-04 15:07
 */
public class Singleton {

    private static volatile Singleton instance = null;

    //防御反射攻击
    private Singleton() {
        if (instance != null) {
            throw new RuntimeException();
        }
    }

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
