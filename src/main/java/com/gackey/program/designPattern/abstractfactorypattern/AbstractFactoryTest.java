/*
 * title: AbstractFactoryTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.abstractfactorypattern;

/**
 * Desc: 抽象工厂  提供一个创建一系列相关或相互依赖对象的接口，而无须指定它们具体的类。
 * ClassName: AbstractFactoryTest
 * author: gackey
 * date: 2018-11-04 21:18
 */
public class AbstractFactoryTest {

    public static void main(String[] args) {
        User user = new User();
        Product product = new Product();
        // mysql
        // user
        Ifactory mysqlFactory = new MysqlFactory();
        Iuser mysqlU = mysqlFactory.createUser();
        mysqlU.insert(user);
        mysqlU.select(1);
        System.out.println("**********");
        // product
        Iproduct mysqlP = mysqlFactory.createProduct();
        mysqlP.insert(product);
        mysqlP.select(3);
        System.out.println("---------------------------");
        // sql server
        // user
        Ifactory sqlserverFactory = new SqlserverFactory();
        Iuser sqlserverU = sqlserverFactory.createUser();
        sqlserverU.insert(user);
        sqlserverU.select(1);
        System.out.println("**********");
        // product
        Iproduct sqlserverP = sqlserverFactory.createProduct();
        sqlserverP.insert(product);
        sqlserverP.select(3);

    }

}
