/*
 * title: IteratorTest.java
 * copyright:©2018-2020 gackey. All Rights Reserved.
 */

package com.gackey.program.designPattern.iteratorpattern;

/**
 * Desc: 迭代器模式  提供一种方法顺序访问一个聚合对象中各个元素，而又不暴露该对象的内部表示。
 * ClassName: IteratorTest
 * author: gackey
 * date: 2018-11-24 20:03
 */
public class IteratorTest {

    public static void main(String[] args) {

        ConcreteAggregate ca = new ConcreteAggregate();
        ca.setItem("gackey");
        ca.setItem("ninjaFrog");
        ca.setItem("Look Crazy");
        ca.setItem("Whatever Zhang");
        ca.setItem("Fault Young");
        ca.setItem(9527);

        SelfIterator si = new ConcreteIterator(ca);
        Object first = si.first();
        System.out.println("第一位的名字是：" + first);

        System.out.println("依次报出名字！");
        while (!si.beDone()) {
            System.out.println(si.currentItem());
            si.next();
        }
    }

}
