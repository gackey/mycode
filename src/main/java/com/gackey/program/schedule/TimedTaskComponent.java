package com.gackey.program.schedule;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author gackey
 * @desc todo
 * @className TimedTaskComponent
 * @date 2023/8/14 19:43
 */
@Component
public class TimedTaskComponent {

    @Scheduled(cron = "*/2 * * * * ?")
    public void outputTask() {
        for (int i = 0; i < 100; i++) {
            System.out.println("定时任务第" + i + "次输出");
        }
    }
}
