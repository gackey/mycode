package com.gackey.program;

import com.gackey.program.algorithm.SnowFlake;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProgramApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void testSnowFlake() {
        long id = SnowFlake.nextId();
        System.out.println(id);
    }

}
